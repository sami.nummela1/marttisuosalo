import sys

class MarttiSuosalo(object):

    def __init__(self):
        self.players = 0
        self.rounds = 0
        self.playerList = []

    def startGame(self):
        arguments = sys.argv[1:]
        self.setupGame(arguments)
        self.playGame()

    def setupGame(self,arguments):
        if len(arguments) != 2:
            self.exitProgram("Plase give exactly 2 arguments: number of players and number of rounds")
        try:
            self.players = int(arguments[0])
            self.rounds = int(arguments[1])
            if(self.players < 1 or self.rounds < 1):
                self.exitProgram("There must be at least 1 player and 1 round")
            self.playerList = list(range(1,self.players+1))
        except:
            self.exitProgram("Arguments must be valid integers")

    def playGame(self):
        print("nPlayers: "+str(self.players)+" nRounds: "+str(self.rounds))
        for turn in range(self.rounds):
            actualTurn = turn+1
            marttiSuonsalo = self.getMarttiSuonsalo(actualTurn)
            self.playRound(actualTurn, marttiSuonsalo, self.playerList[0])
            self.playLogic(actualTurn)

    def playLogic(self,turn):
        changeDirection = self.checkDigits(turn)
        self.switchPlayer(changeDirection)

    def playRound(self, turn, marttiSuonsalo, player):
        if(marttiSuonsalo == True):
            self.printTrunDetailsToConsole("Martti Suosalo", player)
        else:
            self.printTrunDetailsToConsole(turn, player)

    def switchPlayer(self,changeDirection):
        if(changeDirection):
            currentPlayer = self.playerList.pop(0)
            self.playerList.reverse()
            self.playerList.append(currentPlayer)
        else:
            currentPlayer = self.playerList.pop(0)
            self.playerList.append(currentPlayer)

    def checkDigits(self,turn):
        #change direction if 11, 22, etc
        if(len(set(str(turn))) == 1 and len(str(turn)) > 1):
            return True
        else:
            return False

    def getMarttiSuonsalo(self,turn):
        if(turn % 7 == 0):
            # divisible by 7
            return True
        elif('7' in str(turn)):
            # contains 7 as one of its digits
            return True
        elif(sum([int(x) for x in str(turn)]) == 7):
            # the sum of the digits is seven
            return True
        else:
            return False

    def printTrunDetailsToConsole(self,msg,player):
        print("P: ",str(player), " \"" +str(msg)+"\"")

    def exitProgram(self, message='Exiting program'):
        print(message)
        exit(0)

if __name__ == '__main__':
    s = MarttiSuosalo()
    s.startGame()